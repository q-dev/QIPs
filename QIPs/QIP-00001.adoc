# Fully Decentralized Governance Layer for Web3

[cols="1,1"]
|===
| Creation Date | 2022-10-06 
| Latest Update | 2022-11-24
| Contributors  | @TLatzke, Martin Schmidt
| Status        | Draft
|===

## Summary

Governance in Web2 is broken. Decentralized governance beyond code-is-law does not yet exist. Q aims to fix this by providing a comprehensive and fully decentralized governance layer for Web3. 

## Rationale 

Today, decentralized systems largely operate on a code-is-law basis. But this is severely limiting, since this governance paradigm can only deal with deterministic decision types. Wherever more sophisticated decisions are required, blockchains and applications built on top of these largely fall back on “old-world” solutions: Either they rely on nation-state-based legal frameworks, which works poorly in connection with Web3; or they revert back to centralized solutions, where a small group of insiders effectively calls the shots. 

This bug is blatantly evident in Web3 today. The symptoms: Governance exploits, rug pulls, toxic community debates and an inability to implement complex, long-term and truly value-adding use cases in Web3.

## Specification

Q aims to fix this. The protocol provides a governance layer that goes beyond code-is-law: Builders can design applications with a rule-set that captures nuance and intent, allowing non-binary decisions to be made in a decentralized way. Q is open-source, permissionless and independent. It enables creative minds to develop and execute completely new use cases.

### Definition of Done

This bug can be closed when an ecosystem of applications has emerged on Q, demonstrating that good governance in the decentralized world adds value to the people building and using such applications. 