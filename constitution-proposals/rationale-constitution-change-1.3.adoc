{empty} +
[.text-left]
== Constitution Proposal

{empty} +
[cols="50%,50%", frame=none, grid=none]
|===
|Constitution Version | 1.3
|New Hash | link:./4fbd55592b6d327e7fbba78b4843ffab528df04779a376ff068974b2e39f6f3f.adoc[0x4fbd55592b6d327e7fbba78b4843ffab528df04779a376ff068974b2e39f6f3f]
|===

{empty} +
[.text-left]
[horizontal]
=== Proposed Changes
[horizontal.text-left]
This set of proposed changes to the Q Constitution focuses on the Decentralized Finance part of the Q ecosystem: there are changes aimed at clarifying and aligning between the Q Constitution and the technical implementations of DeFi on Q; there are also changes which introduce new elements, namely the introduction of an explicit scope of discretion of the DeFi expert panel and Q stablecoins.

Other non DeFi-related changes include minor new elements such as slashing eligibility periods and intention of the Root Node panel, as well as minor tidying up of the text and miscellaneous alignments of the tech to the Constitution text.

{empty} +
[.text-left]
[horizontal]
==== DeFi-related clarifications and alignment between tech and Constitution text

*Lending and Borrowing-related changes*

Changes are concentrated in Appendix 4. Section 1 (a) (vii) clarifies the process of the Collateral Liquidation Auction. Section 2 clarifies the meaning of Collateral Ceiling. Section 5 defines System Surplus. Section 6 defines System Debt. Section 7 re-describes surplus auctions, as the previous description was inaccurate. Section 8 describes System Debt Auctions.

The description of Collateral Ceiling is introduced to provide greater clarity in Appendix 8. System Surplus and System Debt are introduced as operational concepts within Q DeFi so that people can better understand the system and what they are entitled to.

{empty} +
[.text-left]
[horizontal]
==== New DeFi-related elements

*Introduction of QEUR and the concept of Q stablecoins*

This proposed change replaces the term QUSD with Q stablecoins throughout the Constitution in anticipation of the potential launch of stablecoins not linked to USD such as QEUR. These changes are namely in Appendix 1, Appendix 4, Appendix 7, Appendix 8.

This change also clarifies in Appendix 4 section 1(i) that stablecoins have different eligible collateral and collateralization ratios.

This proposal includes the implementation of https://gitlab.com/q-dev/QIPs/-/blob/015102654eef09072cf7fabe7ca74d580aa473a1/QIPs/QIP-00011.adoc[QIP-11].

*Limitations on how DeFi parameters are to be set*

The scope of discretion of members of the DeFi Expert Panel is introduced in Appendix 8, section 1(b). This provision should mitigate the risk of attack to the DeFi system via malicious actors proposing unreasonable changes to DeFi parameters. This provision seeks to mitigate risk by improving Root Node supervision over DeFi Expert Panel proposals.

When assessing the Constitutionality of a DeFi parameter change proposal, Root Nodes can refer to these new provisions to determine the Constitutionality of such a proposal. With respect to certain DeFi parameters, the proposer is also expected to demonstrate their thinking so that Root Nodes may also assess the reasonableness of the new proposed parameter.

{empty} +
[.text-left]
[horizontal]
==== Tidying up of the text

*Changing “slashing node” to “slashed node” in 5.3.6 and 6.7*

*Changing “Rood node” to “Root Node” in Appendix 2, Part A, 1(v) and 6.12*

*Amendment to definitions of Root Node List and Validator Node Exclusion List*

{empty} +
[.text-left]
[horizontal]
==== Non DeFi-related clarifications and alignment between tech and Constitution text

*Separating the L0 process from on-chain voting as separate sections, clarifying the L0 process, the Validator Node List, the Validator Node Exclusion List, and Root Node List*

This change introduces a distinction between the On-chain Voting Proposal and Layer Zero Voting Proposal, introduces a new Appendix 6, and deletes the definition of Voting Proposals. Further, it introduces revisions to Art 9 and removes line items from the table in Para 3 of Appendix 5 relating to the Validator Node List and the Validator Node Exclusion List. Finally, it introduces clarifications relating to participation of rights of Validators and Root Nodes following changes to the Validator Node Exclusion List and the Root Node List via Art. 4.9, Art 5.2, and Art 5.3.14.

The current live version of the Q Constitution already contemplates that certain communications between the Root Nodes and related voting would happen off-chain, rather than on-chain.

The rationale for off-chain communications and voting is that, in case of collusion between a number of Validator Nodes and/or Root Nodes which compromises the security of the Q Blockchain, the ability of the Root Nodes to communicate with each other and to vote on and maintain the Root Node List and the Validator Node Exclusion List should remain unaffected. The maintenance of the Validator Node Exclusion List is particularly important, as that controls who validates new blocks on the Q Blockchain.

However, there has been feedback from the root node community that the distinction between on-chain and off-chain voting – both in terms of which type of voting should be used when and how the processes differ – is not always clear in the live version of the Q Constitution.

Therefore, in this proposed version of the Q Constitution, the existing concept of Voting Proposal has been replaced with the concept of On-chain Voting Proposal, to make it clear such proposals always relate to on-chain voting.

In addition, in the context of Art 9 and the maintenance of the Root Node List and the Validator Node Exclusion List, Layer Zero Voting Proposal has been introduced as a standalone concept. The rules for layer zero voting have been added in a new standalone Appendix 6. These rules are a simplified version of the rules for on-chain voting in Appendix 5.

Significantly, the Q Constitution does not prescribe the exact mechanism for off-chain voting to be followed. That is deliberately left technology-neutral and it is expected that the mechanism – or, indeed, multiple mechanisms – may change over time as the system evolves.

The proposed amendments to Art 9 are intended to correct two mismatches between the language of the Q Constitution and the technical implementation of the relevant mechanisms.

First, the Validator Node List is a list which is generated by determining which Q Nodes have the highest Stake (i.e., that is a technical/non-discretionary assessment) and disregarding/removing from that group those (former) Validator Nodes whose Main Accounts are included on the Validator Node Exclusion List. Therefore, when the Root Nodes remove a Validator Node from the panel of Validator Nodes in accordance with Art 9.1, what happens as a matter of technical implementation is that they add the Main Account of such Validator Node to the Validator Node Exclusion List.

Secondly, the decision by the Root Nodes to add the Main Account of a Validator Node to the Validator Node Exclusion List is taken by way of a Layer Zero Voting Proposal, rather than an On-chain Voting Proposal. Hence, that has been clarified in Art 9.1.

In addition to these main changes, two further small amendments are proposed to Art 9. The wording in Art 9.2 is rephrased to make it clear that each individual Root Node has a positive obligation to formally update the Validator Node List from time to time. Further, the last sentence of Art 9.3 has been deleted, as it substantively overlaps with the third sentence of Art 5.2.

Art 4.9 makes it clear that, when the Main Account of a (former) Validator Node is removed from the Validator Node Exclusion List following a Layer Zero Voting Proposal which is accepted in accordance with Appendix 6, if such (former) Validator Node has a sufficiently high Stake, it may become a Validator Node again.

The amendments to Art 5.2 are intended to clarify that, as soon as the Main Account of a Root Node is removed from the Root Node List, such Root Node is no longer entitled to vote on any On-chain Voting Proposals or Layer Zero Voting Proposals in its capacity as a Root Node – including on any such proposals initiated before such Root Node’s removal.

Finally, the changes to Art 5.3.14 make it explicit that there may be circumstances under the Q Constitution where a Root Node is required to exercise its veto right in respect of an On-chain Voting Proposal, regardless of whether such Root Node considers such On-chain Voting Proposal to be in compliance with the Q Constitution.

*Clarifying the difference between public keys and Q addresses*

The change to the definition of Public Key clarifies that, as a matter of technical implementation, in certain circumstances only the rightmost 160 bits of the hash of such public key would be used/visible.

*Adding an obligation for Root Nodes to implement contract upgrade proposals"*

This textual change highlights the obligation of Root Nodes to assess amendments to source code in Art 5.3.10, amendment to Para 2(h) of Appendix 5 and amendment to Art 5.5. These amendments are explanatory in nature and align the wording of the Q Constitution with how Q currently operates.

The newly added Art 5.3.10 and Para 2(h) of Appendix 5 formally require the Root Nodes, as part of their overall duties, to compare the proposed source code and the terms of the corresponding On-chain Voting Proposal, to make sure that what is being implemented technically matches what has been approved by the Q Token Holders.

Although the Q Constitution does not impose this as a strict requirement, we expect that, in practice, any On-chain Voting Proposal for such amendments or technical changes would already contain the suggested technical implementation and/or sufficient accompanying explanations, to facilitate the Root Nodes' decision. Otherwise, the Q Stakeholder who makes the relevant On-chain Voting Proposal would run the risk that such amendments or technical changes are not implemented because the Root Nodes cannot get comfortable that the proposed source code matches the terms of the relevant On-chain Voting Proposal.

The amendment to Art 5.5 follows on from the above. Since the implementation of the changes will commence as soon as more than 50% of the Root Nodes vote in favour – and once implementation commences voting is no longer possible – the language in Art 5.5 has be revised to make it clear that, in such a scenario, any Root Nodes who have not yet voted would not be in technical breach of their obligation under Art 5.3.10.

*Changing the maximum number of standby validators from 32 (incorrect) to 34 (what is technically implemented)*

{empty} +
[.text-left]
[horizontal]
==== New non DeFi-related elements

*Adding a provision on the intention for the Root Node panel*

Para 1 of Part B of Appendix 2 has been expanded to state the purpose why the Root Node Selection Expert Panel is rating the various Root Node Candidates in accordance with certain criteria. Ultimately, this goes back to one of the key principles underpinning Q, namely that, in order to perform their function as guardians of the system, the panel of Root Nodes should be independent, resistant to outside interference, exhibiting a broad range of experience, technically competent and driven to promote Q.

*Creating tiered slashing eligibility periods for Root Nodes and validator nodes*

This proposal introduces a limitation period for Slashing Proposals in Art 6.1, a definition of Slashing Proposal Eligibility Period, as well as new line items in Part B of Appendix 7.

This is a conceptually new point. Essentially, it is proposed that, if a Root Node or a Validator Node breach their obligations under the Q Constitution, they can only be sanctioned with Slashing if a Slashing Proposal is made and accepted in accordance with Art 6 during a prescribed period after the breach. This period has been named a Slashing Proposal Eligibility Period. The mechanism is intended to give certainty to the various Q Stakeholders that, for example, in case of a breach committed by a Root Node or a Validator Node early on in their "career", their potential liability for such breach is not unlimited in time. The onus is therefore on the (other) Root Nodes or Q Token Holders to identify and initiate Slashing in respect of any breach(es) sufficiently promptly.

This is analogous to the concept of limitation periods which exists in many legal systems – for example, in case of a contractual breach by one party to a contract, the other party has a certain period of time during which it can seek formal redress.

The Slashing Proposal Eligibility Periods applicable for different breaches are set out in Part B of Appendix 7. The periods for more severe breaches are longer than the periods for less severe breaches. The thinking behind this is that the consequences of more severe breaches are likely to be more significant and the Root Nodes may need longer time to gather evidence – and even prepare for potential challenges by arbitration – before they initiate Slashing.
